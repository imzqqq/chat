# `certbot --nginx -d example.com`

###############################################################################################
# Basic usage
server {
    listen 80;
    server_name example.com;
	
    location / {
        root  /src/project;
        index index.html;
    }

    location /api {
        proxy_pass http://localhost:port;
    }
}

###############################################################################################
# Advanced usage
server {
	listen 443 ssl http2;
	listen [::]:443 ssl http2;

	# For the federation port
	listen 8880 ssl http2 default_server;
	listen [::]:8880 ssl http2 default_server;

	server_name chat.imzqqq.top;

	ssl_protocols TLSv1.2 TLSv1.3;
	ssl_ciphers HIGH:!MEDIUM:!LOW:!aNULL:!NULL:!SHA;
	ssl_prefer_server_ciphers on;
	ssl_session_cache shared:SSL:10m;
	ssl_session_tickets off;

	location ~* ^(\/chat|\/_chat\/client) {
		proxy_pass http://localhost:8080;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header Host $host;

		# Nginx by default only allows file uploads up to 1M in size
		# Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
		client_max_body_size 1024M;
	}

	location ~* ^(\/_chat\/admin) {
		proxy_pass http://localhost:8080;
		proxy_set_header X-Forwarded-For $remote_addr;
		proxy_set_header X-Forwarded-Proto $scheme;
		proxy_set_header Host $host;

		# Nginx by default only allows file uploads up to 1M in size
		# Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
		client_max_body_size 1024M;
	}

	location /.well-known/matrix/client {
		default_type application/json;
		add_header Access-Control-Allow-Origin *;
		return 200 '{"m.homeserver": {"base_url": "https://chat.imzqqq.top"}, "im.vector.riot.e2ee": {"default": false}}';
	}

	###### Use certbot to generate ssl_certificate and ssl_certificate_key ######
}

###############################################################################################
server{
	listen 443 ssl;
	server_name www.imzqqq.top;

	ssl on;
	ssl_session_timeout 5m;
	ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
	ssl_ciphers ECDHE-RSA-AES128-GCM-SHA256:HIGH:!aNULL:!MD5:!RC4:!DHE;
	ssl_prefer_server_ciphers on;

	###### Manaully add ssl_certificate and ssl_certificate_key ######
	ssl_certificate /etc/nginx/5007246_imzqqq.top.pem;
	ssl_certificate_key /etc/nginx/5007246_imzqqq.top.key;

	root /root/workspace/_WE_SHOP/weshop_admin/dist;
	index index.html;

	location / {
		proxy_pass  http://localhost:8008;
		proxy_set_header    Host    $host;
		proxy_set_header    X-Real-IP   $remote_addr;
		proxy_set_header    X-Forwarded-For $proxy_add_x_forwarded_for;
	}
}

###############################################################################################
# Redirect http requests to https
server {
	listen 80;
	server_name imzqqq.top;
	rewrite https://$server_name$request_uri? permanent;
}

or 

server {
		listen 80;
		server_name imzqqq.top;
		rewrite  ^(.*) https://$server_name$request_uri? permanent;
}

###############################################################################################
upstream  git{
    # The domain_name corresponds to the external_url in the gitlab configuration,
    # and the port corresponds to nginx['listen_port'] in the gitlab configuration
    server  domain_name:port;
}

server{
	#if ($host = gitlab.imzqqq.top) {
        #return 301 https://$host$request_uri;
    #} # managed by Certbot

    listen 80;
    # This domain_name is the access address provided to the end user
    server_name domain_name;

    location / {
        # This size setting is very important, if there are large files in the git repository 
		# and the setting is too small, the git push will fail, adjust it according to the situation
        client_max_body_size 4096m;

        proxy_redirect off;
        # The following ensures that the url of the project in gitlab is the domain_name 
		# and not http://git, which is indispensable
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        # Reverse proxy to gitlab's built-in nginx
        proxy_pass http://git;
        index index.html index.htm;
    }

	server {
			listen 443 ssl http2;
			listen [::]:443 ssl http2;

			server_name domain_name;

			ssl_protocols TLSv1.2 TLSv1.3;
			ssl_ciphers HIGH:!MEDIUM:!LOW:!aNULL:!NULL:!SHA;
			ssl_prefer_server_ciphers on;
			ssl_session_cache shared:SSL:10m;
			ssl_session_tickets off;

			location / {
					client_max_body_size 4096m;
					proxy_redirect off;
					proxy_set_header Host $host;
					proxy_set_header X-Real-IP $remote_addr;
					proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
					proxy_pass http://git;
					index index.html index.htm;
			}

		###### Use certbot to generate ssl_certificate and ssl_certificate_key ######

	}
}
