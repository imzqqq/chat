chat-server-py3 (0.34.0) stable; urgency=medium

  chat-server-py3 is intended as a drop-in replacement for the existing
  chat-server package. When the package is installed, chat-server will be
  automatically uninstalled. The replacement should be relatively seamless,
  however, please note the following important differences to chat-server:

  * Most importantly, the chat-server service now runs under Python 3 rather
    than Python 2.7.

  * Chat is installed into its own virtualenv (in /opt/venvs/chat-server)
    instead of using the system python libraries. (This may mean that you can
    remove a number of old dependencies with `apt autoremove`).

  * If you have previously manually installed any custom python extensions
    (such as chat-server-rest-auth) into the system python directories, you
    will need to reinstall them in the new virtualenv. Please consult the
    documentation of the relevant extensions for further details.

  chat-server-py3 will take over responsibility for the existing
  configuration files, including the chat-server systemd service.

  Beware, however, that `apt purge chat-server` will *disable* the
  chat-server service (so that it will not be started on reboot), even
  though that service is no longer being provided by the chat-server
  package. It can be re-enabled with `systemctl enable chat-server`.

  The matrix.org team will continue to provide Python 2 `chat-server`
  packages for the next couple of releases, to allow time for system
  administrators to test the new packages.

 -- Richard van der Hoff <richard@matrix.org>  Wed, 19 Dec 2018 14:00:00 +0000
