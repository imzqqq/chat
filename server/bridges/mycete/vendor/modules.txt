# github.com/BurntSushi/toml v0.3.1
## explicit
# github.com/ChimeraCoder/anaconda v2.0.0+incompatible
## explicit
github.com/ChimeraCoder/anaconda
# github.com/ChimeraCoder/tokenbucket v0.0.0-20131201223612-c5a927568de7
## explicit
github.com/ChimeraCoder/tokenbucket
# github.com/PuerkitoBio/goquery v1.8.0
## explicit; go 1.13
# github.com/andybalholm/cascadia v1.3.1
## explicit; go 1.16
# github.com/azr/backoff v0.0.0-20160115115103-53511d3c7330
## explicit
github.com/azr/backoff
# github.com/btittelbach/cachetable v0.9.1
## explicit; go 1.12
github.com/btittelbach/cachetable
# github.com/cpuguy83/go-md2man/v2 v2.0.1
## explicit; go 1.11
# github.com/dustin/go-jsonpointer v0.0.0-20160814072949-ba0abeacc3dc
## explicit
github.com/dustin/go-jsonpointer
# github.com/dustin/gojson v0.0.0-20160307161227-2e71ec9dd5ad
## explicit
github.com/dustin/gojson
# github.com/fatih/color v1.13.0
## explicit; go 1.13
# github.com/garyburd/go-oauth v0.0.0-20180319155456-bca2e7f09a17
## explicit
github.com/garyburd/go-oauth/oauth
# github.com/gokyle/goconfig v0.0.0-20150908043511-373746557f7f
## explicit
github.com/gokyle/goconfig
# github.com/gorilla/websocket v1.4.2
## explicit; go 1.12
github.com/gorilla/websocket
# github.com/matrix-org/gomatrix v0.0.0-20190418155519-0c31efc5dc73 => /home/imzqqq/workspace/chat/server/chat/go/gomatrix
## explicit; go 1.12
github.com/matrix-org/gomatrix
# github.com/mattn/go-colorable v0.1.11
## explicit; go 1.13
# github.com/mattn/go-isatty v0.0.14
## explicit; go 1.12
# github.com/mattn/go-mastodon v0.0.4-0.20190311051807-e804ee7eb264 => /home/imzqqq/workspace/chat/server/chat/python/bridges/mycete/go-mastodon
## explicit; go 1.12
github.com/mattn/go-mastodon
# github.com/mattn/go-runewidth v0.0.6
## explicit; go 1.9
# github.com/mattn/go-tty v0.0.3
## explicit; go 1.14
# github.com/microcosm-cc/bluemonday v1.0.2
## explicit; go 1.9
github.com/microcosm-cc/bluemonday
# github.com/pmezard/go-difflib v1.0.0
## explicit
# github.com/russross/blackfriday/v2 v2.1.0
## explicit
# github.com/shurcooL/sanitized_anchor_name v1.0.0
## explicit
# github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80
## explicit
github.com/tomnomnom/linkheader
# github.com/urfave/cli v1.22.5
## explicit; go 1.11
# golang.org/x/crypto v0.0.0-20190308221718-c2843e01d9a2
## explicit
# golang.org/x/net v0.0.0-20211101193420-4a448f8816b3
## explicit; go 1.17
golang.org/x/net/context
golang.org/x/net/html
golang.org/x/net/html/atom
# golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
## explicit
# golang.org/x/sys v0.0.0-20211103235746-7861aae1554b
## explicit; go 1.17
golang.org/x/sys/internal/unsafeheader
golang.org/x/sys/unix
# golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1
## explicit; go 1.11
# golang.org/x/text v0.3.6
## explicit; go 1.11
# golang.org/x/tools v0.0.0-20180917221912-90fa682c2a6e
## explicit
# gopkg.in/check.v1 v0.0.0-20161208181325-20d25e280405
## explicit
# gopkg.in/yaml.v2 v2.2.2
## explicit
# github.com/matrix-org/gomatrix => /home/imzqqq/workspace/chat/server/chat/go/gomatrix
# github.com/mattn/go-mastodon => /home/imzqqq/workspace/chat/server/chat/python/bridges/mycete/go-mastodon
